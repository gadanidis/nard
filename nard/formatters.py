import datetime as dt
from typing import Dict, Any

import markdown
import pandas as pd
from praw.models import Comment, Submission


def format_comment_html(comment: Comment, op: str) -> str:
    # Indicate if it's the OP.
    if comment.author == op:
        author = f'<span class="author_op">{comment.author}</span>'
    else:
        author = f'<span class="author">{comment.author}</span>'

    score = comment.score
    body = markdown.markdown(comment.body)

    # Note this doesn't contain the closing </li>
    return f'<li><div class="comment"><p>{author} ({score})</p><p>{body}</p></div>'


def format_replies_html(comment: Comment, op: str) -> str:
    html_comment = [format_comment_html(comment, op)]
    if comment.replies:
        html_comment.append("<ul>")
        for reply in comment.replies:
            html_comment.append(format_replies_html(reply, op))
            html_comment.append("</li>")
        html_comment.append("</ul>")
        html_comment.append("</li>")
    return "".join(html_comment)


def format_submission_html(submission: Submission) -> str:
    html = []

    # Title
    html.append("<html><head>")
    html.append(f"<title>{submission.title} - /r/{submission.subreddit}</title>")
    html.append("</head><body>")
    html.append('<link rel="stylesheet" href="../style.css"/>')
    html.append(f"<h1>{submission.title} ({submission.id})</h1>")

    # Metadata
    html.append("<h2>Metadata</h2><ul>")
    html.append(f"<li> Author: {submission.author}</li>")
    html.append(
        "<li> Date: "
        f'{dt.datetime.fromtimestamp(submission.created_utc).strftime("%Y/%m/%d")}</li>',
    )
    html.append(f"<li>Score: {submission.score}</li></ul>")

    # A submission always has selftext or a link
    if submission.selftext:
        html.append("<h2>Selftext</h2>")
        html.append(f"<p>{markdown.markdown(submission.selftext)}")
    else:
        html.append("<h2>Link</h2>")
        html.append(f'<p><a href="{submission.url}">{submission.url}</a></p>')
        if submission.url.split(".")[-1] in ["png", "jpg", "jpeg", "gif"]:
            html.append(f'<img src="{submission.url}">')

    html.append("<h2>Comments</h2>")
    html.append('<ul class="nested">')
    for comment in submission.comments:
        html.append(format_replies_html(comment, submission.author))
    html.append("</ul>")
    html.append('<a href="../index.html">Back to index</a>')
    html.append("</body></html>")
    return "".join(html)


def format_submission_csv(submission: Submission) -> pd.DataFrame:
    def comment_to_row(comment: Comment) -> Dict[str, Any]:
        row = {
            "author": comment.author,
            "type": "comment",
            "title": None,
            "body": comment.body,
            "link": None,
            "score": comment.score,
            "id": comment.id,
            "submission_id": comment.submission.id,
            "parent": comment.parent_id,
            "level": "top_comment" if comment.parent_id[:2] == "t3" else "reply",
            "subreddit": comment.subreddit.name,
            "permalink": "https://www.reddit.com" + comment.permalink,
            "unix_time": comment.created_utc,
            "date_utc": dt.datetime.utcfromtimestamp(comment.created_utc).strftime("%Y-%m-%d"),
            "time_utc": dt.datetime.utcfromtimestamp(comment.created_utc).strftime("%H:%M:%S"),
            "edited": comment.edited,
        }
        return row

    first_row = {
        "author": submission.author,
        "type": "self_post" if submission.is_self else "link_post",
        "title": submission.title,
        "body": submission.selftext if submission.is_self else None,
        "link": None if submission.is_self else submission.url,
        "score": submission.score,
        "id": submission.id,
        "submission_id": submission.id,
        "parent": None,
        "level": "submission",
        "subreddit": submission.subreddit.name,
        "permalink": "https://www.reddit.com" + submission.permalink,
        "date_utc": dt.datetime.utcfromtimestamp(submission.created_utc).strftime("%Y-%m-%d"),
        "time_utc": dt.datetime.utcfromtimestamp(submission.created_utc).strftime("%H:%M:%S"),
        "edited": submission.edited,
    }

    comment_list = submission.comments
    rows = [first_row] + [comment_to_row(comment) for comment in comment_list]

    return pd.DataFrame(rows)
