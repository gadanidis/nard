from nard.config import provide_config
from nard.download import download_all


def main():
    config = provide_config()
    download_all(config)


if __name__ == "__main__":
    main()
