import argparse
import getpass
import sys
from pathlib import Path
from types import SimpleNamespace
from typing import MutableMapping

import toml
from xdg.BaseDirectory import xdg_config_home


def read_config_file() -> MutableMapping[str, str]:
    xdg_config_path = Path(xdg_config_home) / "nard" / "nard_config.toml"

    if xdg_config_path.is_file():
        config = toml.load(xdg_config_path)
    elif Path("nard_config.toml").is_file():
        config = toml.load("nard_config.toml")
    else:
        print(
            f"Error: Could not find 'nard_config.toml' at {xdg_config_path} "
            "or in the current directory.\n"
            "You need to create a config file to use nard; "
            "check the README if you need help!",
        )
        sys.exit(1)

    return config


def get_config_file_value(key: str, config_file: MutableMapping[str, str]) -> str:
    try:
        return config_file[key]
    except KeyError:
        print(f"Error: Could not find a value for '{key}' in your config file.")
        sys.exit(1)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Download data from a subreddit for linguistic/discourse analysis.",
    )
    parser.add_argument("subreddit", help="Subreddit name (not including '/r/').")
    parser.add_argument("start", help="Start date in format 'yyyy-mm-dd'")
    parser.add_argument("end", help="End date in format 'yyyy-mm-dd'")
    parser.add_argument(
        "-t",
        "--html",
        action="store_true",
        help="Whether to save the data as HTML.",
    )
    parser.add_argument(
        "-s",
        "--csv",
        action="store_true",
        help="Whether to save the data as CSV.",
    )
    parser.add_argument(
        "-p",
        "--pickle",
        action="store_true",
        help="Whether to save the data as pickles.",
    )
    parser.add_argument(
        "-k",
        "--keep-deleted",
        action="store_true",
        help="Whether to keep submissions that are deleted or removed (default: false)",
    )
    parser.add_argument(
        "-c",
        "--css",
        default="light",
        help=(
            "CSS colour theme (only applied for HTML)."
            "Can be 'light' or 'dark' (default: 'light')."
        ),
    )
    parser.add_argument(
        "-o",
        "--output",
        default="reddit_data",
        help="Directory in which to store output.",
    )
    return parser.parse_args()


def provide_config() -> SimpleNamespace:
    config_file = read_config_file()
    args = parse_arguments()

    # Save in HTML, CSV and pickle by default
    if not any([args.pickle, args.html, args.csv]):
        print(
            "You did not specify any formats to save the data, so saving in "
            "HTML, CSV and pickle by default.",
        )
        save_csv, save_pickle, save_html = [True, True, True]
    else:
        save_csv, save_pickle, save_html = [args.csv, args.pickle, args.html]

    # Validate CSS theme
    if save_html:
        if args.css not in ["light", "dark"]:
            print("Error: Unrecognized CSS colour theme.")
            sys.exit(1)

    config = SimpleNamespace(
        subreddit=args.subreddit,
        start=args.start,
        end=args.end,
        save_csv=save_csv,
        save_pickle=save_pickle,
        save_html=save_html,
        keep_deleted=args.keep_deleted,
        css=args.css,
        output=args.output,
        client_id=get_config_file_value("client_id", config_file),
        client_secret=get_config_file_value("client_secret", config_file),
        user_agent=get_config_file_value("user_agent", config_file),
        username=get_config_file_value("username", config_file),
        password=getpass.getpass(prompt=f"Password for /u/{config_file['username']}: "),
    )

    return config
