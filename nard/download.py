import datetime as dt
import pickle
import shutil
import sys
from pathlib import Path
from types import SimpleNamespace
from typing import Dict, Generator

import praw
from praw.models import Submission
from prawcore import OAuthException
from psaw import PushshiftAPI

from nard.formatters import format_submission_csv, format_submission_html


def get_ids(api: PushshiftAPI, subname: str, start_str: str, end_str: str) -> Generator:
    """Accesses the Pushshift API to get a list of IDs for the user's query."""
    start = int(dt.datetime.strptime(start_str, "%Y-%m-%d").timestamp())
    end = int((dt.datetime.strptime(end_str, "%Y-%m-%d") + dt.timedelta(days=1)).timestamp())

    search = api.search_submissions(subreddit=subname, filter=["id"], after=start, before=end)
    return search


def set_up_directory(config: SimpleNamespace) -> Dict[str, Path]:
    """Sets up the directories where data will be saved.

    Only creates directories that will be used. Returns the directory paths in a dictionary for
    later reference.
    """

    # set up data directories and create them if they don't exist
    data_dir = Path(config.output)

    pickle_dir = data_dir / "pickles"
    if config.save_pickle and not pickle_dir.is_dir():
        pickle_dir.mkdir(parents=True)

    csv_dir = data_dir / "csv"
    if config.save_csv and not csv_dir.is_dir():
        csv_dir.mkdir(parents=True)

    html_dir = data_dir / "html"
    if config.save_html and not html_dir.is_dir():
        html_dir.mkdir(parents=True)
        if not (html_dir / "submissions").is_dir():
            (html_dir / "submissions").mkdir()

    if config.save_html:
        # Set the CSS theme
        if config.css == "dark":
            css_file = Path(__file__).parent / "resources" / "style_dark.css"
        else:
            css_file = Path(__file__).parent / "resources" / "style_light.css"
        shutil.copy(css_file, html_dir / "style.css")

        # Create HTML index
        with (html_dir / "index.html").open("w") as f:
            f.write("<html>\n")
            f.write("<head>\n")
            f.write('<link rel="stylesheet" href="style.css"/>\n')
            f.write(f"<title>/r/{config.subreddit}</title>\n")
            f.write("</head>\n")
            f.write("<body>\n")
            f.write("<h1>Index</h1>\n")
            f.write("<ol>\n")

    return {"pickle": pickle_dir, "csv": csv_dir, "html": html_dir}


def log_in_to_reddit(config) -> praw.Reddit:
    reddit = praw.Reddit(
        client_id=config.client_id,
        client_secret=config.client_secret,
        password=config.password,
        user_agent=config.user_agent,
        username=config.username,
    )

    # Check the user's credentials
    try:
        reddit.user.me()
    except OAuthException:
        print("Authentication error! -- check your username and password")
        sys.exit(1)

    return reddit


def download_submission(submission: Submission, config: SimpleNamespace, dirs: Dict[str, Path]):
    """Save a single submission in the requested formats."""
    submission_date = dt.datetime.fromtimestamp(int(submission.created_utc)).strftime("%Y-%m-%d")

    sys.stdout.write(f"\033[K...working on submission {submission.id} ({submission_date})...\r")

    # Get all the submission's comments
    submission.comments.replace_more(limit=0)

    # Write submission as csv
    if config.save_csv:
        df = format_submission_csv(submission)
        df.to_csv(dirs["csv"] / f"{submission.id}.csv")

    # Write submission as HTML
    if config.save_html:
        with open(dirs["html"] / "submissions" / f"{submission.id}.html", "w") as f:
            f.write(format_submission_html(submission))

        # Add submission to HTML index
        with (dirs["html"] / "index.html").open("a") as f:
            f.write(
                f'<li><a href="submissions/{submission.id}.html">{submission.id}: '
                f'{submission.title}</a> (<span class="nobr">{submission_date}</span>)</li>\n',
            )

    # Write submission as pickle
    if config.save_pickle:
        with open(dirs["pickle"] / f"{submission.id}.pickle", "wb") as f:
            pickle.dump(submission, f)


def download_all(config: SimpleNamespace):
    """Search for the submissions and save each, unless removed/deleted."""
    # Initialize PRAW reddit instance
    reddit = praw.Reddit(
        client_id=config.client_id,
        client_secret=config.client_secret,
        password=config.password,
        user_agent=config.user_agent,
        username=config.username,
    )

    # Initialize PSAW api instance
    api = PushshiftAPI(reddit)

    # set up the directories where the data will be stored
    dirs = set_up_directory(config)

    for s_id in get_ids(api, config.subreddit, config.start, config.end):
        submission = reddit.submission(id=s_id)

        if submission.selftext in ["[removed]", "[deleted]"] and not config.keep_deleted:
            continue

        download_submission(submission, config, dirs)

    # End HTML index if applicable
    if config.save_html:
        with (dirs["html"] / "index.html").open("a") as f:
            f.write("</ol></body></html>")

    print("\ndone!")
