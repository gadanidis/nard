**Nard is currently defunct due to Reddit API changes in June 2023.**

---

# nard

A program for downloading data from Reddit for discourse analysis purposes. Nard
glues together [PRAW](https://praw.readthedocs.io/en/stable/) and
[PSAW](https://github.com/dmarx/psaw) in order to download an archive of a given
subreddit during a set period of time with up-to-date comments, post and comment
scores, edits, and so on. Nard *searches* for posts and comments from the
[Pushshift](https://pushshift.io/api-parameters/) database (via PSAW), which is
faster than searching Reddit directly. But it *fetches* posts and comment
content from Reddit directly (via PRAW) so that the data you get reflects the
current state of Reddit.

For instance, if a comment has been deleted on Reddit, it will be deleted in the
data fetched by Nard, which is important from an ethics perspective (if a user
intended for their post to be removed from Reddit, it doesn't seem right to me
to analyze it!). We also get any edits that have been made to the data, as well
as up-to-date post and comment scores. Data downloaded directly from Pushshift
won't reflect these updates because Pushshift just archives posts shortly after
they are made and does not update them later.

Of course, if you are comfortable writing Python code, you can also use PRAW and
PSAW directly in a script of your own; the idea behind Nard is to make the
process of using Reddit data easier for those who are not familiar with
programming.

**NOTE**: Nard is in a non-final state and may not have as many options as you
like. If it does not suit your needs, you are welcome to modify it and share it
[subject to the terms of the
license](https://gitlab.com/gadanidis/nard/-/blob/main/LICENSE). If you have a
request for a specific feature that is not currently implemented you can also
feel free to let me know.

## Installation

1. You need a Reddit account to use Nard, so create one if you don't have one
yet.
2. You also need a Reddit API key and secret. You can sign up here:
<https://www.reddit.com/wiki/api>
3. After signing up for the API, you get your API key and secret by going to
[the apps page](https://www.reddit.com/prefs/apps) and clicking "create an app".
Select 'script' rather than 'web app' or 'installed app'.
4. [Install the latest Python 3](https://www.python.org/downloads/) if you do
not have it already. Nard requires at least Python 3.9.
5. Download Nard by entering `git clone https://gitlab.com/gadanidis/nard.git`
in your terminal, then enter the directory using `cd nard`. If you do not have
`git` installed, you can find instructions for downloading and installing it
here: <https://git-scm.com/downloads>; alternatively you can [download the
repository as a zip
file](https://gitlab.com/gadanidis/nard/-/archive/main/nard-main.zip).
6. I recommend installing in a [virtual
environment](https://docs.python.org/3/library/venv.html) to avoid messing with
your system Python installation. Create a virtual environment by running
`python3 -m venv venv`. Then, you can activate the environment by running
`source venv/bin/activate` (while still in the `nard` folder).
7. To install `nard` in the virtual environment, after activating it run `pip
install .` (the `.` is important; it represents the current directory, which
should be the root directory of the package, i.e., the same directory that
contains this README file).
8. While the virtual environment is activated, you can now run the `nard`
command as described in the 'Usage' section below (you will need to configure it
first; see the 'Configuration' section below). You can deactivate the virtual
environment by running `deactivate` or just closing your terminal. Note that you
will need to reactivate the virtual environment to run `nard` again.

## Configuration

To use Nard you need to have a plain text file named `nard_config.toml` in the
same directory that you are running it, or you can put it in your
`$XDG_CONFIG_HOME/nard` if you know what that means, creating any necessary
directories if they don't exist yet (on macOS, this is
`/Users/[your_user_name]/.config/nard`, on most Linux systems this is
`/home/[your_user_name/.config/nard`).
See `nard_config.toml.sample` in this repository for an example of what your
config file should look like.
Here are the required fields:

- `client_id`: your client ID from the Reddit app you created in the
  installation step
- `client_secret`: your client secret from the Reddit app you created in the
  installation step
- `username`: the Reddit username associated with your Reddit app
- `user_agent`: a short description of what your script is (this is provided to
  Reddit when you access it via Nard)

## Usage

For basic usage, run:

```
nard <subreddit> <start> <end>
```

- `subreddit` is the name of the subreddit (not including "/r/")
- `start` is the first day to collect data from, in ISO 8601 format
- `end` is the last day to collect data (inclusive), in ISO 8601 format

You will be prompted for your Reddit password.

For example, the following command downloads all submissions and comments from
[/r/talesfromyourserver](https://www.reddit.com/r/TalesFromYourServer/) from
March 1, 2020 to August 31, 2021.

```
nard talesfromyourserver 2020-03-01 2021-08-31
```

By default, the data will be saved to a folder called `reddit_data` in the same
directory you run `nard` (so, if you run it from inside the same directory as
Nard is installed, the folder `reddit_data` will be created there).
if you want to change the output directory, pass the `-o` flag.
For example, the following command is the same as the previous but saves in a
folder called `tfys` on the user's Desktop.

```
nard talesfromyourserver 2020-03-01 2021-08-31 -o ~/Desktop/tfys
```

For more information on Nard's options, run `nard --help`.

Note that you should be prepared for the program to run for a long time if you
are downloading a lot of data. Currently pausing and continuing the download is
not supported, but this will be supported in a future version.
For now, if you accidentally stop Nard and need to restart, you can check the
output files to see what date it left off at and rerun the command with that
date as the new start date.

## Output formats

By default, Nard outputs in three formats: CSV, HTML, and 'pickle':

HTML is useful for qualitative analysis and browsing.
You can open the generated `index.html` file and navigate the downloaded data
within your web browser, or you can import the HTML files into qualitative
analysis software such as [Taguette](https://gitlab.com/remram44/taguette).

The CSV output format may be useful if you want to do basic quantitative
analysis.

[Pickle](https://docs.python.org/3/library/pickle.html) is a format for
serializing Python objects and may be useful if you want to use the data in a
Python script of your own.

## FAQ

1. Does Nard stand for something?

No. But if you want a recursive acronym you could pretend it stands for
"Nard's A Reddit Downloader" or "Nard Archives Reddit Discourse". Or you
could imagine it as a cry of dismay: "not another Reddit downloader!" But none
of those are official names for this project, since Reddit does not allow apps
using their API to use the name "Reddit" in their title unless it is preceded by
the word "for" (e.g., "Nard for Reddit").

2. How do I cite Nard?

Something like this should work:

> Gadanidis, Tim (2021). Nard: A data downloader for Reddit. Retrieved from
> <https://gitlab.com/gadanidis/nard>.

Please also cite [PRAW](https://praw.readthedocs.io/en/stable/) and
[PSAW](https://github.com/dmarx/psaw), which handle all the actual hard work of
downloading and searching Reddit, as well as
[Pushshift](https://ojs.aaai.org/index.php/ICWSM/article/view/7347), the dataset
used for searching. Nard is essentially just a wrapper that glues them together
and outputs the data in formats convenient for discourse analysis.
